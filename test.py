import datetime
import calendar

# get the month's days as tuples (x, y) where x is the date and y is day name
month = [d for d in calendar.Calendar().itermonthdays2(2021, 12) if d[0] != 0]

people = {
        'a' : [1,3,6],    
        'b' : [2,4,8],    
        'c' : [1,3,7,9],    
        # 'd' : [1,3,7,9],    
        # 'e' : [1,3,7,9],    
        # 'f' : [1,3,7,9],    
        # 'g' : [1,3,7,9],    
        # 'h' : [1,3,7,9],    
        # 'i' : [1,3,7,9],    
        # 'j' : [1,3,7,9],    
    }

# def check(perm, x, v, pos):
#     print(f"perm:{perm}\nx:{x}\nv:{v}\npos:{pos}")
#     return perm.count(x) <= 3 and perm[pos-1] != x and perm[pos] not in v

def first_permutation(people):
    '''
    people: dict with [key]:[value] = [unique name value]:[list of forbidden dates]
    '''
    first_permutation = []
    for p, v in people.items():
        if 1 not in v:
            first_permutation.append(p)

    return first_permutation

counter = 0
def get_iterations(s):
    global counter
    counter += 1
    # print(counter, s)
    iter_list = []

    for sub in s:
        for p, v in people.items():
            try:
                if sub.count(p) < 3 and sub[len(sub)-1] != p and len(sub)+1 not in v:
                    # input(f"current sequence: {sub}\n canditate: {p}\n count of {p} in sequence: {sub.count(p)}\n canditate position: {len(sub)+1}\n possible positions for canditate:{v}\n")
                    iter_list.append(sub+p)
                # if check(sub, p, v, len(sub)):
                #     iter_list.append(sub+p)
            except:
                pass

    if iter_list == []:
        return s
    elif counter == 30:
        return iter_list
    else:
        return get_iterations(iter_list)

permutations = get_iterations(first_permutation(people))
print(permutations)
