from calendar import Calendar
# from functools import lru_cache
# from datetime import date


class Solver:
    def __init__(self, month, people):
        self.month = [d for d in Calendar().itermonthdays2(
            2022, month) if d[0] != 0]
        self.counter = len(self.month)
        self.people = people
        self.sk_index = {}
        self.distance = 8
        self.distance_step = 1


    # s: the current array
    # p: the person
    # v: the person forbidden list of dates
    def _checks(self, sub, p, v):
        check = True
        check = check and sub.count(p) < 4
        if not check:
            return check
        check = check and len(sub)+1 not in v
        if not check:
            return check
        for i in range(self.distance):
            try:
                check = check and sub[len(sub)-i] != p
                if not check:
                    return check
            except:
                pass

        return check

    def _check_sk(self, sub, p):
        counter = 0
        for day, person in enumerate(sub):
            if person == p:
                weekday = self.month[day][1]
                if weekday == 5 or weekday == 6:
                    counter += 1

        weekday = self.month[len(sub)+1][1]
        if weekday == 5 or weekday == 6:
            counter += 1

        if counter > 1:
            # print("found double SK!!!!!", p, sub)
            return False

        return True



    def _first_permutation(self):
        _first_permutation = []
        for p, v in self.people.items():
            if 1 not in v:
                _first_permutation.append([p])

        return _first_permutation

    def _get_permutations(self, s):
        # print(self.counter)

        iter_list = []

        for sub in s:
            for p, v in self.people.items():
                try:
                    # if self._checks(sub, p, v):
                    if self._checks(sub, p, v) and self._check_sk(sub, p):
                    # if sub.count(p) < 3 and sub[len(sub)-1] != p and len(sub)+1 not in v:
                        iter_list.append(sub+[p])
                        print(sub+[p], len(sub+[p]))
                except:
                    pass

        if self.counter == 0:
            print("return iter_list ------------------------")
            return iter_list
        if iter_list == []:
            if self.counter > 0:
                if self.distance > 2:
                    self.distance -= self.distance_step
                    print('enter here', self.distance)
                    # self.counter -= 1
                    return self._get_permutations(s)
            print("return s ------------------------")
            return s
        else:
            print("recursion!!!!!!!!!!!!!!!!!!!!!!!!!")
            self.counter -= 1
            return self._get_permutations(iter_list)

    def run(self):
        return self._get_permutations(self._first_permutation())


