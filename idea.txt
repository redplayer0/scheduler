Given the following 

input_dict = {
    "a": [0, 2, 3, 5, 7],
    ...
    ...
    ...
    "j": [3, 5, 9]
}

You are asked to produce all prossible permutations of a list
that contains 30 elements.

Those elements are the keys of the input_dict.

The following rules must apply:

  - Each key has a list of forbidden positions in the output list.
eg.

output_list = ["a", "b", "c", ..... n30] is wrong because "a" is 
in 0 index and 0 is in input_dict["a"]

  - Each element cant appear more than 3 times in the output_list


